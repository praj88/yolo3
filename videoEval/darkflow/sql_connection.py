
# ---------------------- SQL insertion  -------------------------------
import sqlite3
import os

# Function 1) Create or open database connection
def create_or_open_db(filename):
    '''This either creates the main database or opens it'''
    file_exists = os.path.isfile(filename)
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()
    if file_exists:
        #print ''' "{}" database successfully opened '''.format(filename)
        fix_break =1
    else:
        print(''' "{}" database successfully created '''.format(filename))
    return conn


# Function 2)  Create table inside the database
def create_table(database,table_name):
    ''' Inserts a table with the name of the company symbol'''
    conn = create_or_open_db(database)
    cursor = conn.cursor()

    #This command deletes the table should it already exist
    #This is to avoid the table filling becoming cluttered and un-ordered
    #cursor.execute('''DROP TABLE IF EXISTS ''' + table_name)

    #Inserts the table
    cursor.execute(''' CREATE TABLE IF NOT EXISTS ''' + table_name + '''(CameraName TEXT, Date TEXT, Hour INTEGER, Minute INTEGER,
                   Second INTEGER, FrameNumber INTEGER, ObjectDetected INTEGER, NumObjects INTEGER, FrameObjectNum INTEGER, Label TEXT,
                   Confidence REAL, BottomRight_x INTEGER, BottomRight_y INTEGER, TopLeft_x INTEGER,TopLeft_y INTEGER,
                   FrameHeight INTEGER, FrameWidth INTEGER, ResizeFrameHeight INTEGER, ResizeFrameWidth INTEGER,
                   PRIMARY KEY (CameraName, Date, Hour, Minute, Second, FrameNumber,FrameObjectNum))''')
    conn.commit()
    conn.close()
    print(''' Table "{}"  successfully created '''.format(table_name))


# Function 3) Insert video metadata into database table
def insert_video_logs(video_tags,database,table_name):

    '''Main function - inserts the video logs data'''
    conn = create_or_open_db(database)

    video_tags['ObjectDetected']= video_tags['ObjectDetected'].astype(int)
    video_tags['Hour'] = video_tags['Hour'].astype(int)
    video_tags['Minute'] = video_tags['Minute'].astype(int)
    video_tags['Second'] = video_tags['Second'].astype(int)
    video_tags['FrameNumber'] = video_tags['FrameNumber'].astype(int)
    video_tags['NumObjects'] = video_tags['NumObjects'].astype(int)
    video_tags['FrameObjectNum'] = video_tags['FrameObjectNum'].astype(int)
    video_tags['BottomRight_x'] = video_tags['BottomRight_x'].astype(int)
    video_tags['BottomRight_y'] = video_tags['BottomRight_y'].astype(int)
    video_tags['TopLeft_x'] = video_tags['TopLeft_x'].astype(int)
    video_tags['TopLeft_y'] = video_tags['TopLeft_y'].astype(int)
    video_tags['FrameHeight'] = video_tags['FrameHeight'].astype(int)
    video_tags['FrameWidth'] = video_tags['FrameWidth'].astype(int)
    video_tags['ResizeFrameHeight'] = video_tags['ResizeFrameHeight'].astype(int)
    video_tags['ResizeFrameWidth'] = video_tags['ResizeFrameWidth'].astype(int)

    video_tags.to_sql(table_name,conn, index = False, if_exists='append')
    conn.close()
    print(''' Data successfully inserted ''')
