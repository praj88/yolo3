import sys
import argparse
import os
os.chdir('/Users/prajwalshreyas/Desktop/Singularity/DeepLearning/yolo3/keras-yolo3-master/')
from yolo import YOLO, detect_video
from PIL import Image
import io
# from yolo import detect_image as di

local = True

def yolo_img_resize(img_in):

    if local:
        #Image_data = open("/Users/prajwalshreyas/Desktop/Singularity/Lambda/babyMonitor_app/testImages/NkPPwRsVaXobDC4RJXIMINvZjInDl1dtv80qxZGO-PgQt5xwCKzjkQ_2019-01-13_02-50-24.jpg","rb").read()
        #img = Image.open(io.BytesIO(Image_data))#.convert('LA')
        #img_size = img.size
        #width = img_size[0]
        #height = img_size[1]

        crib_topx = 146
        crib_topy = 214
        crib_bottomx = 543
        crib_bottomy = 578
        rotate = -90

        # crib_img = img.crop((400, 0, 1280, 720))
        crib_img = img_in.crop((crib_topx, crib_topy, crib_bottomx, crib_bottomy))
        # rotate crib image
        crib_img_rot = crib_img.rotate(rotate, resample=Image.BICUBIC, expand=True)
        return crib_img_rot



def detect_img(yolo):
    while True:
        img = input('Input image filename:')
        try:
            image = Image.open(img)
            image_crop = yolo_img_resize(image)
        except:
            print('Open Error! Try again!')
            continue
        else:
            r_image = yolo.detect_image(image_crop)
            r_image.show()
    yolo.close_session()

FLAGS = None

if __name__ == '__main__':
    # class YOLO defines the default value, so suppress any default here
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    '''    Command line options    '''
    parser.add_argument('--model', type=str,help='path to model weight file, default ' + YOLO.get_defaults("model_path"))
    parser.add_argument('--anchors', type=str, help='path to anchor definitions, default ' + YOLO.get_defaults("anchors_path"))
    parser.add_argument('--classes', type=str,  help='path to class definitions, default ' + YOLO.get_defaults("classes_path"))
    parser.add_argument('--gpu_num', type=int,help='Number of GPU to use, default ' + str(YOLO.get_defaults("gpu_num")))

    parser.add_argument('--image', default=False, action="store_true",help='Image detection mode, will ignore all positional arguments')
    '''
    Command line positional arguments -- for video detection mode
    '''
    parser.add_argument(
        "--input", nargs='?', type=str,required=False,default='./path2your_video',
        help = "Video input path"
    )

    parser.add_argument(
        "--output", nargs='?', type=str, default="",
        help = "[Optional] Video output path"
    )

    FLAGS = parser.parse_args()

    if FLAGS.image:
        """
        Image detection mode, disregard any remaining command line arguments
        """
        print("Image detection mode")
        if "input" in FLAGS:
            print(" Ignoring remaining command line arguments: " + FLAGS.input + "," + FLAGS.output)
        detect_img(YOLO(**vars(FLAGS)))
    elif "input" in FLAGS:
        detect_video(YOLO(**vars(FLAGS)), FLAGS.input, FLAGS.output)
    else:
        print("Must specify at least video_input_path.  See usage with --help.")
