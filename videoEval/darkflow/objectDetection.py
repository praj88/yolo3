import time
import os
import pandas as pd
import sqlite3
from matplotlib import pyplot as plt
#from darkflow.net.build import TFNet #dark flow
import cv2
#os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/yolo2/videoEval/darkflow')
from sql_connection import * # sql package
from config import *
import json
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
import boto3 # AWS Packages
from boto3.s3.transfer import S3Transfer # AWS Packages
import logging # to get logging capability
import base64
from PIL import Image, ImageFile, ImageFont, ImageDraw
import json
# from yolo3_keras.yolo import YOLO#, detect_video
#import urllib2 as requ
from io import StringIO, BytesIO
import io

local = False


#S3 settings
# logging.warning('S3 Settings')
# logging.warning(AWS_prajwal_credetials['aws_access_key_id'])
# logging.warning(AWS_prajwal_credetials['aws_secret_access_key'])
s3_transfer = S3Transfer(boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key']))
bucket_img  = 'api.data'#config.s3_buckets['img_bucket']


# ---------------------- yolo3 class  -------------------------------
import colorsys
import os
from timeit import default_timer as timer
import numpy as np

# from tensorflow import keras
import tensorflow as tf
from keras import backend as K
# K = keras.backend
from keras.models import load_model
from keras.layers import Input
from PIL import Image, ImageFont, ImageDraw
# print (keras.__version__)
from yolo3_keras.yolo3.model import yolo_eval, yolo_body, tiny_yolo_body
from yolo3_keras.yolo3.utils import letterbox_image
import os
from keras.utils import multi_gpu_model

K.clear_session()

class YOLO(object):
    _defaults = {
        "model_path": 'yolo3_keras/model_data/yolo.h5',
        "anchors_path": 'yolo3_keras/model_data/yolo_anchors.txt',
        "classes_path": 'yolo3_keras/model_data/coco_classes.txt',
        "score" : 0.3,
        "iou" : 0.45,
        "model_image_size" :(416, 416),
        "gpu_num" : 1,
    }

    @classmethod
    def get_defaults(cls, n):
        if n in cls._defaults:
            return cls._defaults[n]
        else:
            return "Unrecognized attribute name '" + n + "'"

    def __init__(self, **kwargs):
        self.__dict__.update(self._defaults) # set up default values
        self.__dict__.update(kwargs) # and update with user overrides
        self.class_names = self._get_class()
        self.anchors = self._get_anchors()
        self.sess = K.get_session()
        self.graph = tf.get_default_graph()
        # self.graph.finalize() # finalize
        self.boxes, self.scores, self.classes = self.generate()

    def _get_class(self):
        classes_path = os.path.expanduser(self.classes_path)
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
        anchors_path = os.path.expanduser(self.anchors_path)
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def generate(self):
        model_path = os.path.expanduser(self.model_path)
        assert model_path.endswith('.h5'), 'Keras model or weights must be a .h5 file.'

        # Load model, or construct model and load weights.
        num_anchors = len(self.anchors)
        num_classes = len(self.class_names)
        is_tiny_version = num_anchors==6 # default setting
        try:
            self.yolo_model = load_model(model_path, compile=False)
            # global graph
            # graph = tf.get_default_graph()
        except:
            self.yolo_model = tiny_yolo_body(Input(shape=(None,None,3)), num_anchors//2, num_classes) \
                if is_tiny_version else yolo_body(Input(shape=(None,None,3)), num_anchors//3, num_classes)
            self.yolo_model.load_weights(self.model_path) # make sure model, anchors and classes match
        else:
            assert self.yolo_model.layers[-1].output_shape[-1] == \
                num_anchors/len(self.yolo_model.output) * (num_classes + 5), \
                'Mismatch between model and given anchor and class sizes'

        print('{} model, anchors, and classes loaded.'.format(model_path))

        # Generate colors for drawing bounding boxes.
        hsv_tuples = [(x / len(self.class_names), 1., 1.)
                      for x in range(len(self.class_names))]
        self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        self.colors = list(
            map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
                self.colors))
        np.random.seed(10101)  # Fixed seed for consistent colors across runs.
        np.random.shuffle(self.colors)  # Shuffle colors to decorrelate adjacent classes.
        np.random.seed(None)  # Reset seed to default.

        # Generate output tensor targets for filtered bounding boxes.
        self.input_image_shape = K.placeholder(shape=(2, ))
        if self.gpu_num>=2:
            self.yolo_model = multi_gpu_model(self.yolo_model, gpus=self.gpu_num)
        boxes, scores, classes = yolo_eval(self.yolo_model.output, self.anchors,
                len(self.class_names), self.input_image_shape,
                score_threshold=self.score, iou_threshold=self.iou)
        return boxes, scores, classes

    def detect_image(self, image):
        # Initialise result data frame
        yolo_result = []

        if self.model_image_size != (None, None):
            assert self.model_image_size[0]%32 == 0, 'Multiples of 32 required'
            assert self.model_image_size[1]%32 == 0, 'Multiples of 32 required'
            boxed_image = letterbox_image(image, tuple(reversed(self.model_image_size)))
        else:
            new_image_size = (image.width - (image.width % 32),
                              image.height - (image.height % 32))
            boxed_image = letterbox_image(image, new_image_size)
        image_data = np.array(boxed_image, dtype='float32')

        print(image_data.shape)
        image_data /= 255.
        image_data = np.expand_dims(image_data, 0)  # Add batch dimension.
        with self.sess.as_default():
            with self.graph.as_default():
                out_boxes, out_scores, out_classes = self.sess.run([self.boxes, self.scores, self.classes], feed_dict={self.yolo_model.input: image_data, self.input_image_shape: [image.size[1], image.size[0]], K.learning_phase(): 0})



        #print('Found {} boxes for {}'.format(len(out_boxes), 'img'))
        #
        # font = ImageFont.truetype(font='font/FiraMono-Medium.otf',
        #             size=np.floor(3e-2 * image.size[1] + 0.5).astype('int32'))
        # thickness = (image.size[0] + image.size[1]) // 300
        if out_classes is None:
            frame_result = {"FrameObjectNum":[],"Label":[],"Confidence":[],"BottomRight_x":[],"BottomRight_y":[],"TopLeft_x":[],"TopLeft_y":[]}
            yolo_result.append(frame_result)

        else:

            for i, c in reversed(list(enumerate(out_classes))):
                predicted_class = self.class_names[c]
                box = out_boxes[i]
                score = out_scores[i]

                label = '{} {:.2f}'.format(predicted_class, score)
                # draw = ImageDraw.Draw(image)
                # label_size = draw.textsize(label, font)

                top, left, bottom, right = box
                top = max(0, np.floor(top).astype('int32'))
                left = max(0, np.floor(left).astype('int32'))
                bottom = min(image.size[1], np.floor(bottom).astype('int32'))
                right = min(image.size[0], np.floor(right).astype('int32'))
                print(label, (left, top), (right, bottom))

                frame_result = {"FrameObjectNum":str(i + 1),
                                "Label":str(predicted_class),
                                "Confidence": str(score),
                                "BottomRight_x":str(right),
                                "BottomRight_y":str(bottom),
                                "TopLeft_x":str(left),
                                "TopLeft_y":str(top)
                                }

                yolo_result.append(frame_result)

        return yolo_result

    def close_session(self):
        self.sess.close()

# load yolo3 model
yolo = YOLO()

# Function to run YOLO 3 on a input image ---------------------------------------------------------
def personDetection_eval_yolo3(image_in):
    yolo_result = yolo.detect_image(image_in)
    return yolo_result

# ---------------------- End of yolo3 class  -------------------------------
#Flask api initialise
app = Flask(__name__)

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt'])#, 'jpg', 'jpeg'])


# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

def upload_s3(filepath, bucket, img_name):
    s3_transfer.upload_file(filepath, bucket, str(img_name))
    logging.warning('image uploaded to S3')


# Object identification in videos with database output ------------------------------------------------------
def process_video(file,camera_name,file_date,file_hour,file_min,file_sec, size,
                  fps, rectangleColor, video_result_path, FLAG_RENDER_VIDEO):

    video_input = cv2.VideoCapture(file)
    num_of_frames = 1000 #int(video_input.get(cv2.CAP_PROP_FRAME_COUNT))
    print('There is {} frames in this video'.format(num_of_frames))

    yolo_result = pd.DataFrame({'ObjectDetected':[],'FrameNumber':[],'NumObjects':[],
                                'FrameObjectNum':[],'Label':[],'Confidence':[],'BottomRight_x':[],
                                'BottomRight_y':[],'TopLeft_x':[],'TopLeft_y':[]})

    for frame_count in range(1,num_of_frames):
        print(frame_count)
        # Grab a single frame of video
        ret, frame = video_input.read()
        height, width, _ = frame.shape
        # width = input_movie.get(3)  # float
        # height = input_movie.get(4) # float
        frame_resize = frame #cv2.resize(frame, size,  interpolation = cv2.INTER_AREA)

        output  = tfnet.return_predict(frame_resize)

        # Create an output movie file (make sure resolution/frame rate matches input video!)
        if FLAG_RENDER_VIDEO == True:
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            output_movie = cv2.VideoWriter(video_result_path, fourcc, fps, size)


        # if there is no object detected within a frame
        if output is None:
            frame_result = {'ObjectDetected': 0,'FrameNumber':frame_count,'NumObjects':0,
                            'FrameObjectNum':[],'Label':[],'Confidence':[],'BottomRight_x':[],
                            'BottomRight_y':[],'TopLeft_x':[],'TopLeft_y':[]}
            yolo_result = yolo_result.append(frame_result, ignore_index=True)

        else:
            # Loop through all the objects within the frame
            for i in range(0,len(output)):
                frame_result = {'ObjectDetected': 1,
                                'FrameNumber':frame_count,
                                'NumObjects':len(output),
                                'FrameObjectNum':i + 1,
                                'Label':output[i].get('label'),
                                'Confidence':output[i].get('confidence'),
                                'BottomRight_x':output[i].get('bottomright').get('x'),
                                'BottomRight_y':output[i].get('bottomright').get('y'),
                                'TopLeft_x':output[i].get('topleft').get('x'),
                                'TopLeft_y':output[i].get('topleft').get('y')
                                }
                # Include bounding boxes for identified objects
                if FLAG_RENDER_VIDEO == True:
                    right = int(output[i].get('bottomright').get('x'))
                    bottom = int(output[i].get('bottomright').get('y'))
                    left = int(output[i].get('topleft').get('x'))
                    top = int(output[i].get('topleft').get('y'))

                    cv2.rectangle(frame_resize, (left, top), (right, bottom), (0, 0, 255), 2)
                    output_label = output[i].get('label')
                    cv2.putText(frame_resize, output_label, (int(left - right/2), int(top)),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,165,255), 2)
                    output_movie.write(frame_resize)

                yolo_result = yolo_result.append(frame_result, ignore_index=True)

        # Video file attributes
        yolo_result['Confidence'] = yolo_result['Confidence'].round(2) # round to 2 decimal places
        yolo_result['ResizeFrameHeight'] = size[1]
        yolo_result['ResizeFrameWidth']  = size[0]
        yolo_result['FrameWidth']  = width
        yolo_result['FrameHeight'] = height
        yolo_result['CameraName'] = camera_name
        yolo_result['Date'] = file_date
        yolo_result['Hour'] = file_hour
        yolo_result['Minute'] = file_min
        yolo_result['Second'] = file_sec

    return yolo_result


# API set up  ---------------------------------------------------------------------------------------------
@app.route('/personDetectionTest3')
def personDetectionTest():
    #count = redis.incr('hits')
    return 'Person Detection API working'


# Object identification in images with JSON or file path input ---------------------------------------------------------
@app.route('/personDetection3', methods=['POST'])
def personDetection():
    try:
        if local:
            Image_data = open("/Users/prajwalshreyas/Desktop/Singularity/dockerApps/yolo2/videoEval/darkflow/person-laptop.jpg","rb").read()
            img = Image.open(io.BytesIO(Image_data))#.convert('LA')
            crib_topx = 146
            crib_topy = 214
            crib_bottomx = 543
            crib_bottomy = 578
            rotate = -90

            # crib_img = img.crop((400, 0, 1280, 720))
            crib_img = img.crop((crib_topx, crib_topy, crib_bottomx, crib_bottomy))
            # rotate crib image
            crib_img_rot = crib_img.rotate(rotate, resample=Image.BICUBIC, expand=True)

            result = personDetection_eval_yolo3(img)
        else:
            if request.method == 'POST':
                logging.warning('Start of Algo')
                result = 'Error in processing, please ensure the format of the file is as per the documentation.'
                # Get the name of the uploaded file
                logging.warning(request)
                # size1 = 1000000
                # size2 = 2000000
                # size3 = 3000000

                #try:
                if request.is_json == False:
                    file = request.files['file']
                    if allowed_file(file.filename):
                        logging.warning(file.filename)

                        # Make the filename safe, remove unsupported chars
                        filename = secure_filename(file.filename)
                        filename = file.filename
                        filename_img = filename.rsplit('.', 1)[0] + '.jpeg'
                        filepath = 'uploads/' + filename_img
                        logging.warning(filename_img)
                        img_data_base64 = file.stream.read()

                        #write image with reduced quality
                        im = Image.open(BytesIO(base64.b64decode(img_data_base64)))
                        img_in = im.convert("RGB")
                        img_in.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)
                        logging.warning('img_data write complete')

                    else:
                        result = "Not one of the allowed file extensions. Please try another file."

                elif request.is_json:

                        # read image data or URL
                        img_json = pd.DataFrame(request.json)
                        logging.warning('dataframe read')
                        img_file_name= img_json['image_file_name'][0] #image file name
                        img_url_data = img_json['image_data'][0] #image data
                        im = Image.open(BytesIO(base64.b64decode(img_url_data)))
                        img_in = im.convert("RGB")

                        filename_img = img_file_name + '.jpeg'
                        filepath = 'uploads/' + filename_img
                        img_in.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)

                        #logging.warning('img_data write complete')
                # upload to s3
                # logging.warning('S3 Upload Parameters:')
                # logging.warning(filepath)
                # logging.warning(bucket_img)
                # logging.warning('docker/'+filename_img)
                # upload_s3(filepath, bucket_img, 'docker/'+filename_img)
                result = personDetection_eval_yolo3(img_in)
                result = json.dumps(result)
                logging.warning(result)
                return result
    except Exception as e:
            logging.warning(e)


# personDetection()


# Function to run YOLO 2 on a jpg input ---------------------------------------------------------
# def personDetection_eval(image_filepath):
#     # Read in the image_data
#     logging.warning('Start of Person Detection')
#
#     # Initialise result data frame
#     yolo_result = []
#
#     # pd.DataFrame({'PersonDetected':[],'NumPeople':[],
#     #                             'FrameObjectNum':[],'Label':[],'Confidence':[],'BottomRight_x':[],
#     #                             'BottomRight_y':[],'TopLeft_x':[],'TopLeft_y':[]})
#
#     # Read the image from disk
#     frame = cv2.imread(image_filepath)
#     height, width, _ = frame.shape
#     frame_resize = frame # cv2.resize(frame, size,  interpolation = cv2.INTER_AREA)
#
#     output  = tfnet.return_predict(frame_resize)
#
#     # if there is no object detected within a frame
#     if output is None:
#         frame_result = {"PersonDetected": str(0),"NumPeople":str(0),
#                         "FrameObjectNum":[],"Label":[],"Confidence":[],"BottomRight_x":[],
#                         "BottomRight_y":[],"TopLeft_x":[],"TopLeft_y":[]}
#         yolo_result.append(frame_result, ignore_index=False)
#
#     else:
#         # Loop through all the objects within the frame
#         for i in range(0,len(output)):
#             frame_result = {"PersonDetected": str(1),
#                             "NumPeople":str(len(output)),
#                             "FrameObjectNum":str(i + 1),
#                             "Label":output[i].get('label'),
#                             "Confidence": str(output[i].get('confidence')),
#                             "BottomRight_x":str(output[i].get('bottomright').get('x')),
#                             "BottomRight_y":str(output[i].get('bottomright').get('y')),
#                             "TopLeft_x":str(output[i].get('topleft').get('x')),
#                             "TopLeft_y":str(output[i].get('topleft').get('y'))
#                             }
#
#             yolo_result.append(frame_result) #, ignore_index=True
#             print(yolo_result)
#
#     logging.warning('Finish of Session')
#     return yolo_result

if __name__ == "__main__":

    if VIDEO_INPUT_FLAG == True:
        create_table(database,table_name)
        # Get all the file names in the video folder
        video_files = os.listdir(videos_folder)[1:] # to skip the first file which is git tracker
        # detect objects in video and write metadata to sql database
        for single_file in video_files:

            #single_file = video_files[0]
            file = videos_folder + '/' + single_file
            file_metadata = single_file.split('_')

            camera_name = file_metadata[0]
            file_date = file_metadata[1]
            file_hour = int(file_metadata[2])
            file_min = int(file_metadata[3])
            file_sec = int(file_metadata[4][:2])

            # Start object tagging
            yolo_result = process_video(file, camera_name, file_date, file_hour, file_min, file_sec, size,
                                        fps, rectangleColor, video_result_path, FLAG_RENDER_VIDEO)

            # write to sql DB
            insert_video_logs(yolo_result,database,table_name)

    if IMAGE_INPUT_FLAG == True:
        app.run(host="0.0.0.0", debug=False, port=5015)


# ---------------------- draw bounding boxes  -------------------------------

# min_x = dict.get('topleft').get('x') /640
# min_y = dict.get('topleft').get('y') /360
# max_x = dict.get('bottomright').get('x') /640
# max_y = dict.get('bottomright').get('y') /360
#
# images = tf.placeholder(tf.float32, shape=[None, height, width, depth], name="input_image")
# boxes = tf.placeholder(tf.float32, shape=[None, 1, 4], name="bounding_boxes")
#
#
# # Draw a box using open CV
# cv2.rectangle(frame_rotate, (left, top), (right, bottom), (0, 0, 255), 2)
#
# # Draw a label with a name below the face
# cv2.rectangle(frame_rotate, (left, bottom - 25), (right, bottom), (0, 0, 255), cv2.FILLED)
# font = cv2.FONT_HERSHEY_DUPLEX
# cv2.putText(frame_rotate, name_age_gender, (left + 6, bottom - 6), font, 0.5, (255, 255, 255), 1)
#
# Plot image and bounding boxes and detect objects
# img = cv2.imread('person-gun.jpg')
# plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
# plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
# plt.show()
# tfnet.return_predict(img)
