FROM waleedka/modern-deep-learning

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

RUN apt-get update
RUN yes | apt-get install python3-tk

RUN pip install jupyter_kernel_gateway

# Add directory
ADD /videoEval /videoEval

# Install necessary packages
RUN pip install -r /videoEval/requirements.txt

# Expose ports
EXPOSE 5014

# Change working directory
WORKDIR /videoEval/darkflow/
CMD python3 objectDetection.py
